[![pipeline status](https://gitlab.com/Liental/swifty/badges/master/pipeline.svg)](https://gitlab.com/Liental/swifty/commits/master)

# Swifty
Swifty is an application used to shift the time in SRT files, it's fast and simple to use.

### Usage
Swifty is a command-line application and consists of 2 arguments and 2 optional arguments placed after the command.

Structure:
<code>swifty \<filename> \<hh:mm:ss,mmm> [optional-arguments]</code> \
Example:
<code>swifty source.srt 00:00:01,000 --path ./result/new_subtitles.srt</code>


| Argument    | Usage                           |
|-------------|---------------------------------|
| --path      | set result path of the file     |
| --backwards | set the time to be shifted back |
