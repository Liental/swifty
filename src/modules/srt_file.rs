use core::fmt;
use core::fmt::Display;
use core::str::FromStr;

use crate::modules::time::Time;
use crate::modules::subtitle::Subtitle;
use crate::modules::subtitle::SubtitleErrorEnum;

pub struct SrtFile {
    pub subtitles: Vec<Subtitle>
}

impl SrtFile {
    pub fn len (&self) -> usize {
        self.subtitles.len()
    }

    pub fn shift (&mut self, time: &Time, shift_back: bool) {
        for sub in &mut self.subtitles {
            sub.shift(&time, shift_back);
        }
    }

    pub fn list (&self) -> String {
        let mut result = String::new();

        for sub in &self.subtitles {
            let s = sub.to_string();

            result.push_str(s.as_str());
            result.push_str("\r\n");
        }

        result
    }
}

impl FromStr for SrtFile {
    type Err = SubtitleErrorEnum;

    fn from_str (content: &str) -> Result<Self, Self::Err> {
        let mut subs = Vec::new();

        let clear_content = content.replace("\u{feff}", "");
        let sections: Vec<&str> = clear_content.split("\r\n\r\n")
                                         .filter(|x| !x.is_empty())
                                         .collect();

        for section in sections {
            let sub = Subtitle::from_str(section)?;
            subs.push(sub);
        }

        Ok(Self { subtitles: subs })
    }
}

impl Display for SrtFile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let result = self.list();

        write!(f, "{}", result)
    }
}

#[cfg(test)]
mod srt_test {
    use super::*;

    #[test]
    fn srt_to_string () {
        let content = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n\r\n2\r\n00:00:05,500 --> 00:00:10,000\r\nASTONISHING I KNOW\r\n\r\n";
        let expected = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n2\r\n00:00:05,500 --> 00:00:10,000\r\nASTONISHING I KNOW\r\n";
        let srt = SrtFile::from_str(content).unwrap();

        assert_eq!(expected, srt.to_string());
    }

    #[test]
    fn srt_from_str () {
        let content = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n\r\n2\r\n00:00:05,500 --> 00:00:10,000\r\nASTONISHING I KNOW\r\n\r\n";
        let expected = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n2\r\n00:00:05,500 --> 00:00:10,000\r\nASTONISHING I KNOW\r\n";        
        let srt = SrtFile::from_str(content).unwrap();

        assert_eq!(expected, srt.to_string());
    }

    #[test]
    fn shift_forwards () {
        let content = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n\r\n2\r\n00:00:05,500 --> 00:00:10,000\r\nASTONISHING I KNOW\r\n\r\n";
        let expected = "1\r\n00:00:05,000 --> 00:00:10,000\r\nIM A TEST\r\n2\r\n00:00:10,500 --> 00:00:15,000\r\nASTONISHING I KNOW\r\n";
        
        let mut srt = SrtFile::from_str(content).unwrap();
        let time = Time::from_str("00:00:05,000").unwrap();

        srt.shift(&time, false);
        assert_eq!(expected, srt.to_string());
    }

    #[test]
    fn shift_backwards () {
        let content = "1\r\n00:00:05,000 --> 00:00:10,000\r\nIM A TEST\r\n\r\n2\r\n00:00:10,500 --> 00:00:15,000\r\nASTONISHING I KNOW\r\n\r\n";
        let expected = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n2\r\n00:00:05,500 --> 00:00:10,000\r\nASTONISHING I KNOW\r\n";
        
        let mut srt = SrtFile::from_str(content).unwrap();
        let time = Time::from_str("00:00:05,000").unwrap();

        srt.shift(&time, true);
        assert_eq!(expected, srt.to_string());
    }
}
