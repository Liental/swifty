use core::str::FromStr;
use core::convert::From;
use core::num::ParseIntError;

use core::fmt::Display;
use core::fmt;

pub struct Time {
    hours: u32,
    minutes: u32,
    seconds: u32,
    milli: u32
}

impl Time {
    pub fn new () -> Self {
        Self { hours: 0, minutes: 0, seconds: 0, milli: 0 }
    }

    pub fn get_ms (&self) -> u32 {
        let seconds_milli = self.seconds * 1000;
        let minutes_milli = self.minutes * 1000 * 60;
        let hours_milli = self.hours * 1000 * 60 * 60;

        self.milli + seconds_milli + minutes_milli + hours_milli
    }    

    pub fn shift (&self, time_to_add: &Self, shift_back: bool) -> Self {
        let ms = self.get_ms();
        let add_ms = time_to_add.get_ms();
        
        if !shift_back {
            return Self::from(ms + add_ms);
        } 

        let result = ms.saturating_sub(add_ms);
        Self::from(result)
    }
}   

impl From<u32> for Time {
    fn from (milli: u32) -> Self {
        let mut time_left = milli;
        
        let minutes_milli = 1000 * 60;
        let hour_milli = 1000 * 60 * 60;

        let hours = time_left / hour_milli;  
        time_left -= hours * hour_milli;

        let minutes = time_left / minutes_milli;
        time_left -= minutes * minutes_milli;

        let seconds = time_left / 1000;
        time_left -= seconds * 1000;

        Self { hours, minutes, seconds, milli: time_left } 
    }
}

impl FromStr for Time {
    type Err = ParseIntError;

    fn from_str (text: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = text.split(":").collect();
        
        let hours: u32 = split[0].parse()?;
        let minutes: u32 = split[1].parse()?;
        
        let sec: Vec<&str> = split[2].split(",").collect();
        
        let seconds: u32 = sec[0].parse()?;
        let milli: u32 = sec[1].parse()?;

        Ok(Self { hours, minutes, seconds, milli })
    }
}

impl Display for Time {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:02}:{:02}:{:02},{:03}", 
                self.hours, self.minutes, self.seconds, self.milli)
    }
}

#[cfg(test)]
mod time_test {
    use super::*;

    #[test]
    fn time_from_str () {
        let content = "12:34:56,789";
        let time = Time::from_str(content).unwrap();

        assert_eq!(content, time.to_string());
    }

    #[test]
    fn time_from_ms () {
        let ms = 45296789;
        let time = Time::from(ms);

        assert_eq!("12:34:56,789", time.to_string());
    }

    #[test]
    fn time_shift_forwards () {
        let time = Time::from_str("12:34:56,789").unwrap();
        let time_to_shift = Time::from_str("03:25:04,011").unwrap();

        let result = time.shift(&time_to_shift, false);
        
        assert_eq!("16:00:00,800", result.to_string());
    }

    #[test]
    fn time_shift_backwards () {
        let time = Time::from_str("12:34:56,789").unwrap();
        let time_to_shift = Time::from_str("02:34:56,789").unwrap();
        
        let result = time.shift(&time_to_shift, true);
        
        assert_eq!("10:00:00,000", result.to_string());
    }

    #[test]
    #[should_panic]
    fn time_from_str_panic () {
        Time::from_str("00:00").expect("Failed to initialise time struct");
    }
}
