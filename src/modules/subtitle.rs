use crate::modules::time::Time;

use core::fmt;
use core::fmt::Display;
use core::str::FromStr;
use core::num::ParseIntError;

pub struct Subtitle {
    pub id: u32,
    pub time_start: Time,
    pub time_end: Time,
    pub content: String
}

#[derive(Debug)]
pub enum SubtitleErrorEnum {
    ParseIntError(ParseIntError),
    UnexpectedEof
}

impl From<ParseIntError> for SubtitleErrorEnum {
    fn from(e: ParseIntError) -> Self {
        SubtitleErrorEnum::ParseIntError(e)
    }
}

impl Subtitle {
    pub fn new () -> Self {
        Self { id: 0, time_start: Time::new(), time_end: Time::new(), content: String::new() }
    }

    pub fn shift (&mut self, time_to_add: &Time, shift_back: bool) {
        let time_start = self.time_start.shift(time_to_add, shift_back);
        let time_end = self.time_end.shift(time_to_add, shift_back);

        self.time_start = time_start;
        self.time_end = time_end;
    }
}

impl FromStr for Subtitle {
    type Err = SubtitleErrorEnum;

    fn from_str (content: &str) -> Result<Self, Self::Err> {
        let mut lines = content.lines();

        let id = lines.next().ok_or(SubtitleErrorEnum::UnexpectedEof)?;
        let time = lines.next().ok_or(SubtitleErrorEnum::UnexpectedEof)?;

        let id: u32 = id.trim().parse()?;
        let time: Vec<&str> = time.trim().split(" --> ").collect();

        let time_start = time.get(0).ok_or(SubtitleErrorEnum::UnexpectedEof)?;
        let time_end = time.get(1).ok_or(SubtitleErrorEnum::UnexpectedEof)?;

        let time_start = Time::from_str(time_start)?;
        let time_end = Time::from_str(time_end)?;

        let content: Vec<&str> = lines.collect();
        let content = content.join("\r\n");
        
        Ok(Self { id, time_start, time_end, content })
    }
}

impl Display for Subtitle {
    fn fmt (&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}\r\n{} --> {}\r\n{}", 
              self.id, self.time_start.to_string(), self.time_end.to_string(), self.content)
    }
}

#[cfg(test)]
mod subtitle_test {
    use super::*;

    #[test]
    fn subtitle_to_string () {
        let subtitle = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n\r\n";
        let expected = "1\r\n00:00:00,000 --> 00:00:05,000\r\nIM A TEST\r\n";
        let sub = Subtitle::from_str(subtitle).unwrap();

        assert_eq!(expected, sub.to_string());
    }

    #[test]
    fn shift_forwards () {
        let time = Time::from(500);
        let mut sub = Subtitle::new();

        sub.time_end = Time::from(500);
        sub.shift(&time, false);

        assert!(sub.time_start.get_ms() == 500 && sub.time_end.get_ms() == 1000);
    }

    #[test]
    fn shift_backwards () {
        let time = Time::from(500);
        let mut sub = Subtitle::new();

        sub.time_end = Time::from(500);
        sub.shift(&time, true);

        assert!(sub.time_start.get_ms() == 0 && sub.time_end.get_ms() == 0);
    }
}