use swifty::Config;

fn main () {
    let args: Vec<String> = std::env::args().collect();
    let config = Config::new(&args);

    swifty::run(config);
}
