mod modules;

use std::fs;
use core::str::FromStr;

use crate::modules::time::Time;
use crate::modules::srt_file::SrtFile;

pub struct Config {
    pub filename: String,
    pub time: Time,
    pub path: String,
    pub is_backwards: bool
}

impl Config {
    pub fn new (args: &Vec<String>) -> Self {
        let filename = args.get(1).expect("You have to specify a file you want to shift");
        let time = args.get(2).expect("You have to specify the amount of time in milliseconds you want to shift");

        let filename = filename.to_string();
        let time = Time::from_str(time).unwrap();
        let is_backwards = args.contains(&String::from("--backwards"));

        let path_index = args.iter().position(|arg| arg == "--path"); 
        let path_index = path_index.unwrap_or(filename.len());

        let path = match args.get(path_index + 1) {
            Some(path) => path.clone(),
            None => "result.srt".to_string()
        };

        Self { filename, time, is_backwards, path }
    }
}

pub fn run (config: Config) {
    let file = fs::read_to_string(config.filename)
                  .expect("cannot read provided file");

    let file = file.as_str();

    let mut srt = SrtFile::from_str(&file)
                          .expect("cannot parse provided SRT file");

    if config.is_backwards {
        srt.shift(&config.time, true);
    } else {
        srt.shift(&config.time, false);
    }

    println!("Shifted {} subtitles", srt.len());
    
    fs::write(config.path, srt.list())
       .expect("cannot write result to a file");
}

#[cfg(test)]
mod lib_tests {
  use super::*;

  #[test]
fn new_config () {
    let args = vec![
        String::from("path"), 
        String::from("foo.srt"), 
        String::from("00:00:00,100"), 
        String::from("--backwards"),
        String::from("--path"),
        String::from("boo.srt")
    ];

    let config = Config::new(&args);

    assert_eq!(config.filename, args[1]);
    assert_eq!(config.time.to_string(), args[2].to_string());
    assert_eq!(config.path, args[5]);
    assert!(config.is_backwards);
  }
}